'use strict';

import mongoose from 'mongoose';

var MessengerSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean
});

export default mongoose.model('Messenger', MessengerSchema);
