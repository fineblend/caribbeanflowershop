'use strict';

describe('Directive: headerSmall', function () {

  // load the directive's module and view
  beforeEach(module('caribbeanflowershopApp'));
  beforeEach(module('components/headerSmall/headerSmall.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<header-small></header-small>');
    element = $compile(element)(scope);
    scope.$apply();
    element.text().should.equal('this is the headerSmall directive');
  }));
});
