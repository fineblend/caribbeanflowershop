'use strict';

describe('Component: MerchantComponent', function () {

  // load the controller's module
  beforeEach(module('caribbeanflowershopApp'));

  var MerchantComponent;
  var $rootScope;
  var fs, pg, aut;
  var $q;
  var flowers = [{"_id":"555953273","name":"Charisma","vendorId":"7726","website":"Flora2000.com","shareASaleLink":"http://www.shareasale.com/m-pr.cfm?merchantID=7726&userID=YOURUSERID&productID=555953273","picSmall":"http://images.famousid.com/ProdImg200/8950.jpg","picMed":"http://images.famousid.com/ProdImg300/8950.jpg","price":"142.99","parentCategory":"Gifts/Specialty","category":"Flowers","description":"This luxurious and extravagant collection of a dozen fresh red roses will assure to make someone's day!","lastUpdated":"2016-02-23 01:00:48.323","stockStatus":"instock","provider":"Flora2000","categoryDescription":"Flowers Flower Arrangement Flower Bouquet Gifts Hampers","country":"Anguilla","unknownNumber":"8950-169","unknownNumber2":"0","shareASaleActLink":"http://www.shareasale.com/m-pr.cfm?merchantID=7726&userID=YOURUSERID&atc=1&productID=555953273","shareASaleMobileLink":"http://www.shareasale.com/m-pr.cfm?merchantID=7726&userID=YOURUSERID&mobile=1&productID=555953273","selected":true}];

  var countries = ["Anguilla", "Aruba"];

 // Initialize the controller and a mock scope
  beforeEach(inject(function ($componentController, _$q_, _$rootScope_) {
    $q = _$q_;
    fs = {
      getFlowerCountry: function(){
        return $q.when(flowers);
      }
    };

    pg = {
      getCountryArray : function(){
        return $q.when(countries);
      }
    }

    aut = {
        getCurrentUser : function(){
          return $q.when('user');
        }
    }
    $rootScope = _$rootScope_;
    MerchantComponent = $componentController('merchant', {
      flowersService : fs,
      page :pg
    });
  }));

  it('should have an empty array before activation', ()=>{
    MerchantComponent.flowers.should.exist;
  });

  it('show role should set role property', ()=> {
    MerchantComponent.showRole('merchant');
    MerchantComponent.role.should.equal('merchant');
  });
  it('should set flowers on init', ()=>{
    $rootScope.$apply();
  })
});
