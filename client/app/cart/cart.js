'use strict';

angular.module('caribbeanflowershopApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('cart', {
        url: '/cart',
        abstract: 'true',
        template: '<cart></cart>'
      })
      .state('cart.cart',{
        url:'',
        parent: 'cart',
        templateUrl:'app/cart/cart_cart.html'
      })
      .state('cart.checkout',{
        url:'/cartcheckout',
        parent: 'cart',
        templateUrl:'app/cart/cart_checkout.html'
      })
      .state('cart.payment',{
        url:'/cartpayment',
        parent: 'cart',
        templateUrl:'app/cart/cart_payment.html'
      })
  })
  .run(function($rootScope, $stateParams, $location, $state, Auth, page){
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
        if( (toState.name === 'cart.checkout' || toState.name === 'cart.payment') && (Auth.isLoggedIn() === false )){
          event.preventDefault();
          console.log(toState.name, Auth.isLoggedIn());
          page.setReferrer('cart');
          console.log('referrer',page.getReferrer());
          $state.go('login');
        }

      });
  });
