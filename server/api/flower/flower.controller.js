/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/flowers              ->  index
 * POST    /api/flowers              ->  create
 * GET     /api/flowers/:id          ->  show
 * PUT     /api/flowers/:id          ->  update
 * DELETE  /api/flowers/:id          ->  destroy
 */

'use strict';

import _ from 'lodash';
import Flower from './flower.model';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
  };
}

function saveUpdates(updates) {
  return function(entity) {
    var updated = _.merge(entity, updates);
    return updated.save()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return function(entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode|| 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Flowers
export function index(req, res){
    var query = {
         country : { $in : ['Anguilla',
                            'Aruba',
                            'Bahamas Islands',
                            'Barbados',
                            'Belize',
                            'Bermuda',
                            'Cayman Islands',
                            'Dominica',
                            'Dominican Republic',
                            'French Guiana',
                            'Grenada',
                            'Guadeloupe',
                            'Guyana',
                            'Haiti',
                            'Honduras',
                            'Jamaica',
                            'Saint Kitts and Nevis',
                            'Saint Lucia',
                            'Saint Vincent and the Grenadines ',
                            'Trinidad and Tobago'
        ]}
    };
  return Flower.find(query).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets all flowers from a particular country
export function country(req, res){
  return Flower.find({country : req.params.country}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Flower from the DB
export function show(req, res) {
  return Flower.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Flower in the DB
export function create(req, res) {
  return Flower.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Flower in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  return Flower.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Flower from the DB
export function destroy(req, res) {
  return Flower.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

//Kayode
// add merchant to flowers he wishes to sell

export function merchantUpdate(req, res){
  var merchantId = req.body.id;
  var addArrangementIds = req.body.addIds;
  var removeArrangementIds = req.body.removeIds;
  var addResponses = [];
  var removeResponses = [];
  var allResponses;
  addArrangementIds.forEach((arrangementId)=>{
    addResponses += addMerchantIdToArrangement(merchantId, arrangementId);
    console.log('merchantId', merchantId);
  });
  removeArrangementIds.forEach((arrangementId)=>{
    removeResponses += takeMerchantIdFromArrangement(merchantId, arrangementId);
  });

  allResponses = {  addResponses : addResponses,
                    removeResponses: removeResponses
  };
  res.json(allResponses);
}

function takeMerchantIdFromArrangement(merchantId, arrangementId){
  return Flower.findById(arrangementId).exec()
    .then(arrangement=>{
      if(!arrangement){
        return "Arrangement "+arrangementId+" not found.";
      }
      for(var x = arrangement.merchants.length - 1; x>=0; x--){
        if(arrangement.merchants[x] === merchantId){
          arrangement.merchants.splice(x, 1);
        }
      }
      return arrangement;
    }).then(arrangement=>{
      var id = arrangement._id;
      return Flower.update({_id :id}, arrangement).exec();
    }).then(response=>{
      return JSON.parse(response);
    }).catch(error=>{
      return 'Error removing merchant from arrangement '+ error;
    })
}

function addMerchantIdToArrangement(merchantId, arrangementId){
  return Flower.findById(arrangementId).exec()
    .then(arrangement=>{
      if(!arrangement){
        return "Arrangement "+arrangementId+" not found.";
      }
      if(arrangement.merchants){
        arrangement.merchants.push(merchantId);
      }
      else{
        console.log('set merchant')
        arrangement.merchants = [merchantId];
      }
      return arrangement;
    }).then(arrangement=>{
      var id = arrangement._id;
      var merchants = arrangement.merchants;
      console.log('update add '+ id);
      return Flower.update({_id :id}, {$set:{merchants:merchants}}).exec();
    }).then(response=>{
      console.log('response', response);
      return JSON.parse(response);
    }).catch(error=>{
      return 'Error adding merchant to arrangement '+ error;
    })
}
