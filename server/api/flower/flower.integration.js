'use strict';

var app = require('../..');
import request from 'supertest';

var newFlower;

describe('Flower API:', function() {

  describe('GET /api/flowers', function() {
    var flowers;

    beforeEach(function(done) {
      request(app)
        .get('/api/flowers')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          flowers = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      flowers.should.be.instanceOf(Array);
    });

  });

  describe('POST /api/flowers', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/flowers')
        .send({
          _id : '1',
          name: 'New Flower',
          description: 'This is the brand new flower!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newFlower = res.body;
          done();
        });
    });

    it('should respond with the newly created flower', function() {
      newFlower.name.should.equal('New Flower');
      newFlower.description.should.equal('This is the brand new flower!!!');
    });

  });

  describe('GET /api/flowers/:id', function() {
    var flower;

    beforeEach(function(done) {
      request(app)
        .get('/api/flowers/' + newFlower._id)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          flower = res.body;
          done();
        });
    });

    afterEach(function() {
      flower = {};
    });

    it('should respond with the requested flower', function() {
      flower.name.should.equal('New Flower');
      flower.description.should.equal('This is the brand new flower!!!');
    });

  });

  describe('PUT /api/flowers/:id', function() {
    var updatedFlower;

    beforeEach(function(done) {
      request(app)
        .put('/api/flowers/' + newFlower._id)
        .send({
          name: 'Updated Flower',
          description: 'This is the updated flower!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if (err) {
            return done(err);
          }
          updatedFlower = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedFlower = {};
    });

    it('should respond with the updated flower', function() {
      updatedFlower.name.should.equal('Updated Flower');
      updatedFlower.description.should.equal('This is the updated flower!!!');
    });

  });

  describe('DELETE /api/flowers/:id', function() {

    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete('/api/flowers/' + newFlower._id)
        .expect(204)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when flower does not exist', function(done) {
      request(app)
        .delete('/api/flowers/' + newFlower._id)
        .expect(404)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

  });

});
