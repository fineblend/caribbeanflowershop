'use strict';

import mongoose from 'mongoose';

var FlowerSchema = new mongoose.Schema({
  _id             : String,
  name            : String,
  vendorId        : String,
  shareASaleLink  : String,
  picSmall        : String,
  picMed          : String,
  price           : String,
  description     : String,
  stockStatus     : String,
  provider        : String,
  country         : String,
  active          : Boolean,
  merchants       : Array
});

export default mongoose.model('Flower', FlowerSchema);
