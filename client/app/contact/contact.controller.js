'use strict';

(function(){

class ContactComponent {
  constructor() {
    this.heading = 'Contact Us';
    this.style = '{\'background-image\': \'url(assets/images/contact.jpg)\',      \'background-repeat\':\'no-repeat\',\'background-size\':\'cover\'}';
  }
}

angular.module('caribbeanflowershopApp')
  .component('contact', {
    templateUrl: 'app/contact/contact.html',
    controller: ContactComponent,
    controllerAs: 'contact'
  });

})();
