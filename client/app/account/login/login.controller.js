'use strict';

class LoginController {
  constructor($scope, $state, Auth, page) {
    this.$scope = $scope;
    this.Auth = Auth;
    this.isLoggedIn = Auth.isLoggedIn;
    this.$state = $state;
    this.page = page;

    this.user = {};
    this.errors = {};
    this.submitted = false;


  }

  login(form) {
    this.submitted = true;

    if (form.$valid) {
      this.Auth.login({
          email: this.user.email,
          password: this.user.password
        })
        .then(() => {
          // Logged in, redirect to home
          this.$state.go('main');
        })
        .catch(err => {
          this.errors.other = err.message;
        });
    }
  }

  $onInit(){
    var isLoggedIn = this.Auth.isLoggedIn();
    var referrer = this.page.getReferrer();
    this.$scope.$watch('isLoggedIn', function() {
      if(isLoggedIn == true && referrer === 'cart' ){
        this.page.setReferrer(null);
        console.log('page referrer', this.page.getReferrer());
        this.$state.go('cart.checkout');
      }
    });

  }
}

angular.module('caribbeanflowershopApp')
  .controller('LoginController', LoginController);
