'use strict';

angular.module('caribbeanflowershopApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('country', {
        url: '/country/:country',
        template: '<country></country>'
      });
  });
