'use strict';

angular.module('caribbeanflowershopApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('facebook', {
        url: '/facebook',
        abstract: true,
        template: '<facebook></facebook>',
        authenticate: 'facebook',
        resolve:{
          'check':function($location, Auth, page){
              if(page.getReferrer() === 'cart') {
                console.log("cart from facebook.js");
                $location.path('/cartcheckout');
              }
              else{
                Auth.getCurrentUser().$promise.
                then(response=>{
                  var role = response.role;
                  console.log('role', role);
                  if(role ==='merchant'){
                    $location.path('/merchant');
                  }
                });
              }
          }
        }
      })
      .state('facebook.profile',{
        url:'/fbprofile',
        parent: 'facebook',
        templateUrl:'app/account/facebook/fbprofile.html'
      })
      .state('facebook.dashboard',{
        url:'',
        parent: 'facebook',
        templateUrl:'app/account/facebook/dashboard.html'
    });
  });
