'use strict';

describe('Directive: kaHeaderMedium', function () {

  // load the directive's module and view
  beforeEach(module('caribbeanflowershopApp'));
  beforeEach(module('components/kaHeaderMedium/kaHeaderMedium.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<ka-header-medium></ka-header-medium>');
    element = $compile(element)(scope);
    scope.$apply();
    element.text().should.equal('this is the kaHeaderMedium directive');
  }));
});
