'use strict';

angular.module('caribbeanflowershopApp', ['caribbeanflowershopApp.auth',
                                          'caribbeanflowershopApp.admin',
                                          'caribbeanflowershopApp.constants',
                                          'caribbeanflowershopApp.cart',
                                          'ngCookies',
                                          'ngResource',
                                          'ngSanitize',
                                          'ui.router',
                                          'ui.bootstrap',
                                          'validation.match',
                                          'ngAnimate',
                                          '720kb.socialshare',
                                          'djds4rce.angular-socialshare',
                                          'chart.js',
                                          'angularPayments',
                                          'credit-cards',
                                          'toastr',
                                          'angularMoment'

  ])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
  })
  .run(function($rootScope, $state, $stateParams, page){
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeStart', function(event,next,nextParams, current) {
        var nextName = next.name;
        next.title = page.setTitle(nextName, nextParams).title;
        next.metaContent = page.setMetaContent(nextName, nextParams).metaContent;
    })
  })
  .run(function($FB, $window){
    $FB.init('1771615673095499');
    $window.Stripe.setPublishableKey('pk_test_UcJE05gBJ39RUcX1n2toFirQ');
  });
