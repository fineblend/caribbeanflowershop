'use strict';

var express = require('express');
var controller = require('./flower.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.get('/country/:country', controller.country);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.patch('/:id', controller.update);
router.delete('/:id', controller.destroy);
router.post('/merchant', controller.merchantUpdate);

module.exports = router;
