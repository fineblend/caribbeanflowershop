/**
 * Messenger model events
 */

'use strict';

import {EventEmitter} from 'events';
import Messenger from './messenger.model';
var MessengerEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MessengerEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Messenger.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    MessengerEvents.emit(event + ':' + doc._id, doc);
    MessengerEvents.emit(event, doc);
  };
}

export default MessengerEvents;
