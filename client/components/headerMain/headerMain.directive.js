'use strict';

angular.module('caribbeanflowershopApp')
  .directive('headerMain', function () {
    return {
      templateUrl: 'components/headerMain/headerMain.html',
      restrict: 'EA',
      scope: {
        scroll :'&',
        heading: '=',
        heading2: '=',
        style: '='
      },
      // link: function ($scope) {
      //   $scope.pageScroll = function () {
      //     alert("Hello");
      //     var scrollTop = 2000; // For example
      //     angular.element('#pointer').animate({scrollTop: scrollTop}, 'slow');
      //   };
      // }
    };
  });
