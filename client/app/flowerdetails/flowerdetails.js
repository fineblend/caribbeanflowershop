'use strict';

angular.module('caribbeanflowershopApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('flowerdetails', {
        url: '/flowerdetails/:country/:id',
        template: '<flowerdetails></flowerdetails>'
      });
  });
