'use strict';


function cartService($rootScope){

  var cart = [];


  var cartChange = function(){
    $rootScope.$emit('cartChange');
  }

  var getCartLocalStorage = function(){
    if(localStorage.getItem('cart')){
      cart = JSON.parse(localStorage.getItem('cart'));
    }
      return cart;
  };

  var sendCartLocalStorage = function(cart){
    localStorage.setItem('cart', JSON.stringify(cart));
    return getCartLocalStorage();
  };

  var clearCart = function(){
    cart = [];
    sendCartLocalStorage(cart);
    return cart;
  };

  var addItemToCart = function(item){
    var increment;
    cart = getCartLocalStorage();
    increment = false;
    cart.forEach(element=>{
      if(element.id === item.id){
        var elemQty;
        elemQty = Number(element.qty);
        elemQty += Number(item.qty);
        element.qty = elemQty;
        increment = true;
      }
    });
    if(increment === false){
      cart.push(item);
    }
    sendCartLocalStorage(cart);
    cartChange();
    return getCartLocalStorage();
  };

  var getCartTotalPrice = function(){
    var totalPrice = 0;
    cart = getCartLocalStorage();
    cart.forEach(element=>{
      totalPrice += Number(element.price) * Number(element.qty);
    });
    return totalPrice;
  };

  var setItemQuantityInCart = function(itemId, qty){
    cart = getCartLocalStorage();
    var item = {};
    cart.forEach(element=>{
      if(element.id === itemId){
        element.qty = qty;
        sendCartLocalStorage(cart);
        item = element;
        return item;
      }
    });
    cartChange();
    return item;
  };

  var getItemFromCart = function(itemId){
    cart = getCartLocalStorage();
    var item = {};
    cart.forEach(element=>{
      if(element.id === itemId){
        item = element;
        return item;
      }
    });
    return item;
  };

  var removeItemFromCart = function(itemId){
      cart = getCartLocalStorage();
      cart.forEach((element, index)=>{
        if(element.id === itemId){
          cart.splice(index, 1);
        }
      });
      sendCartLocalStorage(cart);
      cartChange();
      return cart;
  };



  var getTotalItems = function(){
    cart = getCartLocalStorage();
    var totalItems = 0;
    cart.forEach(function(item){
      totalItems += Number(item.qty);
    });
    return totalItems;
  };


  return{
    clearCart : clearCart,
    addItemToCart : addItemToCart,
    getTotalItems : getTotalItems,
    getCartLocalStorage : getCartLocalStorage,
    getCartTotalPrice : getCartTotalPrice,
    setItemQuantityInCart : setItemQuantityInCart,
    removeItemFromCart : removeItemFromCart,
    getItemFromCart : getItemFromCart
  }

}

angular.module('caribbeanflowershopApp.cart', [])
  .factory('cartService', cartService);
