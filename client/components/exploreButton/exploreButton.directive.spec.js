'use strict';

describe('Directive: exploreButton', function () {

  // load the directive's module and view
  beforeEach(module('caribbeanflowershopApp'));
  beforeEach(module('components/exploreButton/exploreButton.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<explore-button></explore-button>');
    element = $compile(element)(scope);
    scope.$apply();
    element.text().should.equal('this is the exploreButton directive');
  }));
});
