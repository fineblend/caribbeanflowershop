'use strict';

function pageService($q) {
  var countryArray, referrer;

  // Service logic
  var setTitle = function(nextName, nextParams) {
    if (nextName === 'country' && nextParams.country === 'Anguilla'){
      return {
        title : 'Anguilla flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Aruba'){
      return {
        title : 'Aruba flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Bahama Islands'){
      return {
        title : 'Bahamas flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Barbados'){
      return {
        title : 'Barbados flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Belize'){
      return {
        title : 'Belize flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Bermuda'){
      return {
        title : 'Bermuda flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Cayman Islands'){
      return {
        title : 'Cayman Islands flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Dominica'){
      return {
        title : 'Dominica flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Dominican Republic'){
      return {
        title : 'Dominica Republic flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='French Guiana'){
      return {
        title : 'French Guiana flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Grenada'){
      return {
        title : 'Grenada flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Guadeloupe'){
      return {
        title : 'Guadeloupe flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Guyana'){
      return {
        title : 'Guyana flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Haiti'){
      return {
        title : 'Haiti flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Honduras'){
      return {
        title : 'Honduras flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Jamaica'){
      return {
        title : 'Jamaica flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Saint Kitts and Nevis'){
      return {
        title : 'Saint Kitts and Nevis flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Saint Lucia'){
      return {
        title : 'Saint Lucia flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Saint Vincent and the Grenadines'){
      return {
        title : 'Saint Vincent and the Grenadines flower delivery from a local flower shop'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Trinidad and Tobago'){
      return {
        title : 'Trinidad and Tobago flower delivery from a local flower shop'
      }
    }
    if (nextName === 'main'){
      return {
        title : 'Flower delivery in the Caribbean'
      }
    }
    if (nextName ==='about'){
      return {
        title : 'Caribbeanflowershop.com and your flower delivery'
      }
    }
    if (nextName ==='contact'){
      return {
        title : 'Contact us about buying or selling flowers in the Caribbean'
      }
    }
    else{
      return{
        title : 'Flower delivery in the Caribbean'
      }
    }
  };

  var setMetaContent = function(nextName, nextParams) {
    if (nextName === 'country' && nextParams.country ==='Anguilla'){
      return {
        metaContent : 'Flower Delivery in Anguilla using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Anguilla.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Aruba'){
      return {
        metaContent : 'Flower Delivery in Aruba using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Aruba.'
      }
    }if (nextName === 'country' && nextParams.country ==='Bahama Islands'){
      return {
        metaContent : 'Flower Delivery in the Bahamas using local florists. Avoid surprises and additional fees. Deliver floral arrangements in the Bahamas.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Barbados'){
      return {
        metaContent : 'Flower Delivery in Barbados using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Barbados.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Belize'){
      return {
        metaContent : 'Flower Delivery in Belize using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Belize.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Bermuda'){
      return {
        metaContent : 'Flower Delivery in Bermuda using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Bermuda.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Cayman Islands'){
      return {
        metaContent : 'Flower Delivery in the Cayman Islands using local florists. Avoid surprises and additional fees. Deliver floral arrangements in the Cayan Islands.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Dominica'){
      return {
        metaContent : 'Flower Delivery in Dominica using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Dominica.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Dominican Republic'){
      return {
        metaContent : 'Flower Delivery in the Dominican Republic using local florists. Avoid surprises and additional fees. Deliver floral arrangements in the Dominican Republic.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='French Guiana'){
      return {
        metaContent : 'Flower Delivery in French Guiana using local florists. Avoid surprises and additional fees. Deliver floral arrangements in French Guiana.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Grenada'){
      return {
        metaContent : 'Flower Delivery in Grenada using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Grenada.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Guadeloupe'){
      return {
        metaContent : 'Flower Delivery in Guadeloupe using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Guadeloupe.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Guyana'){
      return {
        metaContent : 'Flower Delivery in Guyana using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Guyana.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Haiti'){
      return {
        metaContent : 'Flower Delivery in Haiti using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Haiti.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Honduras'){
      return {
        metaContent : 'Flower Delivery in Honduras using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Honduras.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Jamaica'){
      return {
        metaContent : 'Flower Delivery in Jamaica using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Jamaica.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Saint Kitts and Nevis'){
      return {
        metaContent : 'Flower Delivery in Saint Kitts using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Saint Kitts.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Saint Lucia'){
      return {
        metaContent : 'Flower Delivery in Saint Lucia using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Saint Lucia.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Saint Vincent and the Grenadines'){
      return {
        metaContent : 'Flower Delivery in Saint Vincent and the Grenadines using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Saint Vincent and the Grenadines.'
      }
    }
    if (nextName === 'country' && nextParams.country ==='Trinidad and Tobago'){
      return {
        metaContent : 'Flower Delivery in Trinidad and Tobago using local florists. Avoid surprises and additional fees. Deliver floral arrangements in Trinidad and Tobago.'
      }
    }
    else{
      return {
        metaContent : 'We deliver using local flower shops. Avoid surprises and additional fees. Order from anywhere in the world. Deliver with us in the Caribbean.'
      }
    }
  };

var getCountryArray= function(){
    countryArray = ['Anguilla',
      'Aruba',
      'Bahama Islands',
      'Barbados',
      'Belize',
      'Bermuda',
      'Cayman Islands',
      'Dominica',
      'Dominican Republic',
      'French Guiana',
      'Grenada',
      'Guadeloupe',
      'Guyana',
      'Haiti',
      'Honduras',
      'Jamaica',
      'Saint Kitts and Nevis',
      'Saint Lucia',
      'Saint Vincent and the Grenadines ',
      'Trinidad and Tobago'
    ];
    return $q.when(countryArray);
  };


  var setReferrer = function(ref){
    referrer = ref;
    localStorage.setItem('referrer', referrer);
    return referrer;
  };

  var getReferrer = function(){
    var referrer = localStorage.getItem('referrer');
    return referrer;
  };


  return {
    setTitle: setTitle,
    setMetaContent: setMetaContent,
    getCountryArray : getCountryArray,
    setReferrer : setReferrer,
    getReferrer : getReferrer
    }
  }



angular.module('caribbeanflowershopApp')
  .factory('page', ['$q', pageService]);
