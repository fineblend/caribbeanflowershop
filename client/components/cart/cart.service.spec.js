'use strict';

describe('Service: cart', function () {

  // load the service's module
  beforeEach(module('caribbeanflowershopApp.cart'));

  // instantiate service
  var cartService;
  var $rootScope;
  var item, item1, item2;
  var cart;

  beforeEach(inject(function (_cartService_, _$rootScope_) {
    cartService = _cartService_;
    $rootScope = _$rootScope_;
    item1 = {id:'123', price:'2.00', qty: 3};
    item2 = {id:'456', price:'2.00', qty: 1};
    localStorage.clear();
  }));

  it('should exist', function () {
    cartService.should.be.instanceOf(Object);
  });

  it('should add item to cart', function(){
    item = {'id':'123', price:'2.00'};
    cart = cartService.addItemToCart(item);
    cart.should.be.instanceOf(Array).and.have.lengthOf(1);
    cart[0].should.have.keys('id', 'price');
  });

  it('should be able to clear cart', function(){
    cart =  cartService.clearCart();
    cart.should.be.instanceOf(Array).and.have.length(0);
  });

  it('should increment qty if same item added', function(){
    cartService.addItemToCart(item2);
    cartService.addItemToCart(item2);
    cart = cartService.getCartLocalStorage();
    cart[0].qty.should.equal(2);

  });

  it('should get item form cart given id',  function(){
    cartService.addItemToCart(item1);
    cartService.addItemToCart(item2);
    item = cartService.getItemFromCart(item1.id);
    item.should.eql(item1);
  })

  it('should set item quantity in cart', function(){
    cartService.addItemToCart(item1);
    cartService.setItemQuantityInCart(item1.id, 25);
    item = cartService.getItemFromCart(item1.id);
    item.qty.should.equal(25);


  });

  it('should remove an item from the cart', function(){
    cartService.addItemToCart(item1);
    cart = cartService.getCartLocalStorage();
    cart.should.not.be.empty;
    cart = cartService.removeItemFromCart(item1.id);
    cart.should.be.empty;

  });

  it('should return number of of items in cart', function(){
    var qty;
    cartService.addItemToCart(item1);
    cartService.addItemToCart(item2);
    qty = cartService.getTotalItems();
    qty.should.equal(4);

  });

  it('should return total price', function(){
    var totalPrice;
    cartService.addItemToCart(item1);
    cartService.addItemToCart(item2);
    totalPrice = cartService.getCartTotalPrice();
    totalPrice.should.equal(8.00);

  })

});
