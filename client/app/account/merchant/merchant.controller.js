'use strict';

/**
 * @param {{cover:string}} response.facebook
 */

(function(){

class MerchantComponent {
  constructor($http, $location, $resource, $window, toastr, Auth, page, flowersService  ) {
    this.$location = $location;
    this.$window = $window;
    this.shares=0;
    this.Auth = Auth;
    this.page = page;
    this.flowersService = flowersService;
    this.$resource = $resource;
    this.toastr = toastr;
    this.$http = $http;
    this.flowers = []; //what shows on the page
    this.items = [];
    this.countries = [];
    this.flowerIds = [];
    this.flowersToRemove = [];
    this.user = {};
    this.role = '';
    this.name = '';
    this.picture = '';
    this.facebook = {};
    this.heading = 'Merchant Console';
    this.style = '{\'background-image\': \'url(assets/images/trinidad.jpg)\',      \'background-repeat\':\'no-repeat\',\'background-size\':\'cover\'}';

  //merchant graphs
    this.labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    //this.series = ['Revenue'];

    this.data = [
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];

    //end graphs

  }




  toggleSelected(index){
    var _id   = this.flowers[index]._id;
    var name  = this.flowers[index].name;
    var arrangement = {'_id': _id, 'name': name};
    var arrangements = this.user.arrangements;
    if(this.flowers[index].selected === true){
      arrangements.push(arrangement);
    }else{
      arrangements.forEach((element, index)=>{
        if(element._id === arrangement._id){
          arrangements.splice(index, 1);
        }
      });
    }
  }


  onClickCheckFlowerIds(index) {
    var id   = this.flowers[index]._id;
    var inArray = false;

    console.log('id', id);

    this.flowerIds.forEach((flowerId, index)=>{
      if(flowerId === id){
        inArray = true;
        this.flowerIds.splice(index, 1);
        this.flowersToRemove.push(flowerId);
      }
    });

    if(inArray === false){
      this.flowerIds.push(id);

      this.flowersToRemove.forEach((element, index)=>{
        if(element === id){
          this.flowersToRemove.splice(index, 1);
        }
      });
    }
  }

  setProfile() {
    var id = this.user._id;
    this.$http({
      method: 'PUT',
      url: '/api/users/' + id,
      data: this.user
    })
      .then(response => {
          console.log(response);
          this.toastr.info('Profile updated');
        }
        ,error=> console.log(error)
      );

    this.sendAddRemoveIds(this.flowerIds, this.flowersToRemove);
  }


  sendAddRemoveIds(addIds, removeIds){

    console.log('add Ids', addIds, 'removeIds', removeIds);
    var data = {
      id : this.user._id,
      addIds : addIds,
      removeIds : removeIds
    };

    this.$http({
      method: 'POST',
      url: '/api/flowers/merchant',
      data: data
    })
      .then(response => console.log('sendAddRemoveIds response', response),
        error=> console.log('sendAddRemoveIds error', error));
  }


  setCountry(country){
    this.user.facebook.location = country;
  }


  showFlowers(country) {
    this.flowersService.getFlowerCountry(country)
      .then(response => {
          var flowers, arrangements;
          flowers = response;
          arrangements = this.user.arrangements;
          arrangements.forEach(arrangement=>{
            flowers.forEach(flower=>{
              if(flower._id === arrangement._id){
                flower.selected = true;
              }
            });
          });
        return this.flowers = response;
      });
  }

  setRole(role){
    this.user.role = role;
    var id = this.user._id;
    this.$http({
      method: 'PUT',
      url: '/api/users/' + id,
      data: this.user
    })
      .then(response =>{
          console.log(response);
          this.$location.path('/facebook');
        },
        error=> console.log(error));
  }

  showRole(role){
    this.role = role;
  }

  setDelivered(index){
    this.items[index].delivered = true;
    var item = this.items[index];
    var id = item._id;

    this.$http({
      method: 'PUT',
      url: '/api/items/' + id,
      data: item
    })
      .then(response => {
          console.log(response);
          this.toastr.info('Delivery status changed');
        }
        ,error=>{
          console.log(error)
          this.toastr.error('Item not updated.');
        });
  }

  setNotDelivered(index){
    this.items[index].delivered = false;
    this.toastr.info('Delivery status changed');
  }

  sendImage($index){
    var imageValue = this.$window.document.getElementById('photo').files[0];
    console.log('imageValue', imageValue);

    var fd = new FormData();
    fd.append('photo', imageValue);
    console.log('fd', fd);

    this.$http.post('/api/items/image', fd, {
      withCredentials: false,
      headers: {'Content-Type': undefined},
      transformRequest: angular.identity
    })
      .then(response => {
          console.log(response);
        });

  }

  $onInit(){
    this.page.getCountryArray()
      .then(data=>{this.countries = data;});
    this.Auth.getCurrentUser().$promise
      .then(response=>{
        console.log(response);
        this.user = response;
        this.role = response.role;
        this.picture = response.facebook.picture.data.url;
        this.name = response.name;
        this.facebook = response.facebook;
        if(this.user.arrangements){
          var arrangements = this.user.arrangements;
          arrangements.forEach(arrangement=>{
            this.flowerIds.push(arrangement._id);
          })
        }
        if(this.facebook.location){
          var country = this.facebook.location;
          this.showFlowers(country);
        }
        if(response.facebook.cover){
          var coverPhoto = response.facebook.cover.source;
          this.style =  '{\'background-image\': \'url('+coverPhoto+')\',      \'background-repeat\':\'no-repeat\',\'background-size\':\'cover\'}';
        }
      });
    this.$http({
      method: 'GET',
      url: '/api/items'
    })
      .success(data => {
        data.forEach(element=>{
          var address2 = '';
          if(data.recipientAddress2){
            address2 = data.recipientAddress2+', ';
          }
          element.recipientFullAddress = element.recipientAddress+', '
                                          +address2
                                          +element.recipientCity+', '
                                          +element.recipientState+'.';
        });

        data.reverse();
        this.items = data;
        console.log('data', data);
      })
      .error(error => {
        console.log('error', error);
      });


  }
}

angular.module('caribbeanflowershopApp')
  .component('merchant', {
    templateUrl: 'app/account/merchant/merchant.html',
    controller: MerchantComponent,
    controllerAs: 'merchant'
  });

})();
