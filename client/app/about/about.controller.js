'use strict';

(function(){

class AboutComponent {
  constructor($http) {
    this.$http = $http;
    this.heading = 'Our Story';
    this.style = '{\'background-image\': \'url(assets/images/technology.jpg)\'}';
    this.contact = {name:'',
                    title:'',
                    email:'',
                    text:''
                  };
  }

  sendContact() {
    var data = this.contact;

    this.$http({
      method: 'POST',
      url: '/mail/contactMail',
      data: data
    })
      .then((response) => {
          console.log(response);
        },
        error => console.log(error));
  }
}
angular.module('caribbeanflowershopApp')
  .component('about', {
    templateUrl: 'app/about/about.html',
    controller: AboutComponent,
    controllerAs: 'about'
  });

})();
