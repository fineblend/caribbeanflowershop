/**
 * Flower model events
 */

'use strict';

import {EventEmitter} from 'events';
import Flower from './flower.model';
var FlowerEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
FlowerEvents.setMaxListeners(0);

// Model events
var events = {
  'save': 'save',
  'remove': 'remove'
};

// Register the event emitter to the model events
for (var e in events) {
  var event = events[e];
  Flower.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    FlowerEvents.emit(event + ':' + doc._id, doc);
    FlowerEvents.emit(event, doc);
  }
}

export default FlowerEvents;
