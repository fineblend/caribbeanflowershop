'use strict';

var express = require('express');
var controller = require('./stripe.controller');
var orderController = require('../api/order/order.controller');
var mailController = require('../mail/mail.controller.js');
var messengerController = require('../api/messenger/messenger.controller');

var router = express.Router();

router.post('/payment', controller.payment,
                        orderController.setPaid,
                        mailController.deliverMail);


export default router;
