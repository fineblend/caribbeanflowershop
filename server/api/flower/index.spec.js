'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var flowerCtrlStub = {
  index: 'flowerCtrl.index',
  show: 'flowerCtrl.show',
  create: 'flowerCtrl.create',
  update: 'flowerCtrl.update',
  destroy: 'flowerCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var flowerIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './flower.controller': flowerCtrlStub
});

describe('Flower API Router:', function() {

  it('should return an express router instance', function() {
    flowerIndex.should.equal(routerStub);
  });

  describe('GET /api/flowers', function() {

    it('should route to flower.controller.index', function() {
      routerStub.get
        .withArgs('/', 'flowerCtrl.index')
        .should.have.been.calledOnce;
    });

  });

  describe('GET /api/flowers/:id', function() {

    it('should route to flower.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'flowerCtrl.show')
        .should.have.been.calledOnce;
    });

  });

  describe('POST /api/flowers', function() {

    it('should route to flower.controller.create', function() {
      routerStub.post
        .withArgs('/', 'flowerCtrl.create')
        .should.have.been.calledOnce;
    });

  });

  describe('PUT /api/flowers/:id', function() {

    it('should route to flower.controller.update', function() {
      routerStub.put
        .withArgs('/:id', 'flowerCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('PATCH /api/flowers/:id', function() {

    it('should route to flower.controller.update', function() {
      routerStub.patch
        .withArgs('/:id', 'flowerCtrl.update')
        .should.have.been.calledOnce;
    });

  });

  describe('DELETE /api/flowers/:id', function() {

    it('should route to flower.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'flowerCtrl.destroy')
        .should.have.been.calledOnce;
    });

  });

});
