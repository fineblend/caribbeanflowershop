'use strict';

import mongoose from 'mongoose';

var ItemSchema = new mongoose.Schema({
  id : String,
  orderDate : Date,
  deliveryDate: Date,
  merchantCheckDeliveredDate : Date,
  enteredDeliveryDate : Date,
  dateEnteredBy : String,
  approvedDate : Date,
  itemName : String,
  imageUrl : String,
  merchant: String,
  orderId : String,
  deliveryMessage : String,
  deliveryInstructions : String,
  recipientName : String,
  recipientAddress : String,
  recipientAddress2 : String,
  recipientCity : String,
  recipientState : String,
  orderedById : String,
  orderedByName : String,
  photo : String,
  delivered : Boolean,
  approved : Boolean,
});

export default mongoose.model('Item', ItemSchema);
