'use strict';

import {Router} from 'express';
import * as controller from './mail.controller';


var router = new Router();

router.post('/contactMail', controller.contact);


module.exports = router;
