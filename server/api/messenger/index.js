'use strict';

var express = require('express');
var controller = require('./messenger.controller');

var router = express.Router();


router.get('/', controller.index);
router.get('/health', controller.health);
router.get('/check', controller.check);
router.get('/webhook', controller.getWebHook);
router.get('/authorize', controller.authorize);
router.post('/webhook', controller.postWebHook);
router.post('/order', controller.sendOrderMessage);
router.get('/authorize', controller.authorize);
router.put('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);


router.get('/assets/:file', function(req, res){
  var file = req.params.file;
  console.log(file);
  res.sendFile( __dirname+'/assets/'+file);
});

module.exports = router;
