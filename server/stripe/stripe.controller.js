'use strict';

var stripe = require('stripe')('sk_test_lbes17N94tdkK7zKAZDWJpLH');

export function payment(req, res, next){
  var token = req.body.token; // Using Express
  var amount = req.body.amount*100;
// Create a charge: this will charge the user's card
  var charge = stripe.charges.create({
    amount: amount, // Amount in cents
    currency: "usd",
    source: token,
    description: "Example charge"
  }, function(err, charge) {
    if (err) {
      res.json('The card has been declined: '+err);
    }else{
      console.log("charge made");
      //var chargeData = JSON.stringify(charge);
      next();
    }
  });
}
