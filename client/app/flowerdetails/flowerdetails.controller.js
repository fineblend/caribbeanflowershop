'use strict';

(function(){

class FlowerdetailsComponent {
  constructor($stateParams, flowersService, cartService, toastr) {
    this.$stateParams = $stateParams;
    this.flowersService = flowersService;
    this.cartService = cartService;
    this.toastr = toastr;
    this.flower = {};
    this.heading = 'Choose Flowers';
    this.heading2 = 'A beautiful option awaits';
  }

  addItemToCart(item){

    var id = item._id;
    var price = item.price;
    var qty = 1;
    var merchant;
    if(item.merchants && item.merchants.length>0){
      merchant = 'merchant';
    }else{
      merchant = 'other';
    }
    var cartItem = {id:id, price:price, qty: qty, merchant: merchant, item:item};
    if(cartItem.merchant === 'merchant'){
      this.cartService.addItemToCart(cartItem);
    }

    if(cartItem.merchant === 'other'){
      var offSiteSuppliers;
      if(localStorage.getItem('offSiteSuppliers')){
        offSiteSuppliers = JSON.parse(localStorage.getItem('offSiteSuppliers'));
      }else{
        offSiteSuppliers =[];
      }
      offSiteSuppliers.push(cartItem);
      this.offSiteItems = offSiteSuppliers;
      localStorage.setItem('offSiteSuppliers', JSON.stringify(offSiteSuppliers));
    }

    this.toastr.info(item.name+' added to cart');
  }

  $onInit(){
    var id = this.$stateParams.id;
    this.flowersService.getFlowerResource().get({id: id}).$promise
      .then(response => {
        var flower = response;
        flower.shareASaleLink = flower.shareASaleLink.replace(/YOURUSERID/i, '166025');
        this.flower = flower;
      });
  }
}

angular.module('caribbeanflowershopApp')
  .component('flowerdetails', {
    templateUrl: 'app/flowerdetails/flowerdetails.html',
    controller: FlowerdetailsComponent,
    controllerAs: 'details'
  });

})();
