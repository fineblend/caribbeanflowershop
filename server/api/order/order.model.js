'use strict';

import mongoose from 'mongoose';

var OrderSchema = new mongoose.Schema({
  id              : String,
  order           : {},
  paid            : String
});

export default mongoose.model('Order', OrderSchema);
