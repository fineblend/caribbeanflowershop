'use strict';

(function(){

class CartComponent {
  constructor( $http, $filter, $rootScope, $window, Auth, cartService, page, toastr) {
    this.message = 'Hello';
    this.heading = 'Choose Flowers';
    this.heading2 = 'A beautiful option awaits';
    this.$http = $http;
    this.$filter = $filter;
    this.$rootScope = $rootScope;
    this.$window = $window;
    this.Auth = Auth;
    this.toastr = toastr;
    this.cartService = cartService;
    this.page = page;
    this.card = {};
    this.order = {delivery:{},
                  recipient:{},
                  user : {},
                  date:'',
                  items:'',
                  country:'',
                  orderTotal:'' };
    this.date={};
    this.date.format = 'yyyy/MM/dd';
    //this.dateObject = ''; //old ng-model for date input
    this.datePicker = {};
    this.cartItems;
    this.showEmpty;
    this.cartTotal;
    this.country;
    this.countries;
    this.tomorrow;
    this.afterTomorrow;
    this.inlineOptions = {
      customClass: this.getDayClass,
      minDate: new Date(),
      showWeeks: true
    };
    this.dateOptions = {
      dateDisabled: this.disabled,
      formatYear: 'yy',
      maxDate: new Date(2020, 5, 22),
      minDate: new Date(),
      startingDay: 1
    };
    this.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    this.format = this.formats[0];
    this.altInputFormats = ['M!/d!/yyyy'];
    this.popup = {
      opened: false
    };


    this.events = [
      {
        date: this.tomorrow,
        status: 'full'
      },
      {
        date: this.afterTomorrow,
        status: 'partially'
      }
    ];
    this.offSiteItems;

  }

  setTomorrow(){
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    this.tomorrow = tomorrow;
    this.datePicker.dt = tomorrow;
  }

  //Disable weekend selection
  disabled(data){
    var date = data.date,
        mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

  toggleMin(){
      this.inlineOptions.minDate = this.inlineOptions.minDate ? null : new Date();
    this.dateOptions.minDate = this.inlineOptions.minDate;
  };

  open(){
    this.popup.opened = true;
  };


  setDate(year, month, day){
    this.datePicker.dt = new Date(year, month, day);
  };

  getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < this.events.length; i++) {
        var currentDay = new Date(this.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return this.events[i].status;
        }
      }
    }

    return '';
  }

  dateToOrder(){
    this.order.delivery.date = this.datePicker.dt;
    console.log('Delivery date', this.order.delivery.date);
  }

  watchCartTotalPrice(){
    this.$rootScope.$on('cartChange', ()=>{
      if(this.cartService.getTotalItems() > 0)
        this.cartTotal=this.cartService.getCartTotalPrice();
    })

  }

  watchCartItems(){
    this.$rootScope.$on('cartChange', ()=>{
      this.cartItems = this.cartService.getCartLocalStorage();
    })
  }

  incrementQty(cartItem){
    cartItem.qty = Number(cartItem.qty)+1;
    this.cartService.setItemQuantityInCart(cartItem.id,cartItem.qty);
  }

  decrementQty(cartItem){
    cartItem.qty = Number(cartItem.qty)-1;
    this.cartService.setItemQuantityInCart(cartItem.id,cartItem.qty);
  }

  removeItem(cartItem){
    var itemId = cartItem.id;
    this.cartService.removeItemFromCart(itemId);

  }

  getDate(){
    var date = new Date();
    var day = date.getDate();

  }

  createOrder(){
    this.Auth.getCurrentUser().$promise
      .then(response=>{
        console.log(response);
        var user = response;
        this.order.user.id = user._id;
        this.order.user.name = user.name;
        this.order.name = user.name;
        this.order.date = this.$filter('date')(new Date(), 'medium');
        this.order.items  = this.cartItems;
        this.order.country = this.country;
        this.order.orderTotal = this.orderTotal;
        this.order.delivery.date = this.datePicker.dt;

        var data  = this.order;
        console.log('order', this.order);
        this.$http({
          method: 'POST',
          url: '/api/orders',
          data: data
        })
          .then((response )=>{
              console.log(response);
              this.toastr.info('Order created');
              var orderId = response.data._id;
              console.log('orderId', orderId);
              this.cardSubmit(orderId, this.order);
          },
            error=> console.log(error));
      });

  }

  cardSubmit(orderId, order){
    this.$window.Stripe.card.createToken({
      number: this.card.number,
      cvc: this.card.cvc,
      exp_month: this.card.exp_month,
      exp_year: this.card.exp_year
    }, (status, response)=>{
      if(response.error){
        var err = response.error.message;
        return console.log('Token creation error '+err);
      }else{
        var token = response.id;
        console.log('token', token);
        var amount = this.cartTotal;
        var data = {
          token : token,
          amount : amount,
          orderId : orderId,
          order : order
        };
        this.$http({
          method: 'POST',
          url: '/stripe/payment',
          data: data
        })
          .then(response =>{
            console.log(response);
            this.toastr.info('Card Validated');
          },error=> console.log(error) );
      }
    });
  }




  $onInit() {
    var offSiteCollection;
    if(localStorage.getItem('offSiteSuppliers')){
      offSiteCollection = JSON.parse(localStorage.getItem('offSiteSuppliers'));
    }
    this.offSiteItems = offSiteCollection;
    this.cartItems = this.cartService.getCartLocalStorage();
    this.offSiteItems = JSON.parse(localStorage.getItem('offSiteSuppliers'));
    if(this.cartItems.length){
      this.country = this.cartItems[0].item.country;
    }
    this.cartTotal = this.cartService.getCartTotalPrice();
    if((this.cartService.getTotalItems() < 1) && !this.offSiteItems){
      this.showEmpty=true;
    }
    this.watchCartTotalPrice();
    this.watchCartItems();
    this.page.getCountryArray()
      .then(data=>{this.countries = data;});
    //begin date input
    this.setTomorrow();
    this.afterTomorrow = new Date();
    this.afterTomorrow.setDate(this.tomorrow.getDate() + 1);
    //end date input
  }

}


angular.module('caribbeanflowershopApp')
  .component('cart', {
    templateUrl: 'app/cart/cart.html',
    controller: CartComponent,
    controllerAs: 'cart'
  });

})();
