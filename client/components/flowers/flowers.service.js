'use strict';

function flowersService($resource) {
  // Service logic
  // ...
  var flowerResource;
  flowerResource = $resource('/api/flowers/country/:country', {country: '@country'});

  var getFlowerResource = function() {
    return $resource('../api/flowers/:id', {id:'@id'});
  };

  var getFlowerCountry = function(country){
    return flowerResource.query({country: country}).$promise;
  };

  // Public API here
  return {
    getFlowerResource: getFlowerResource,
    getFlowerCountry: getFlowerCountry
  }

}


angular.module('caribbeanflowershopApp')
  .factory('flowersService', flowersService);
