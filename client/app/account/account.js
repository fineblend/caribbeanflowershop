'use strict';

angular.module('caribbeanflowershopApp')
  .config(function($stateProvider) {
    $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'app/account/login/login.html',
        controller: 'LoginController',
        controllerAs: 'vm'
      })
      .state('logout', {
        url: '/logout?referrer',
        referrer: 'main',
        template: '',
        controller: function($state, Auth) {
          var current = $state.current.referrer;
          var referrer;
          if(current.search('facebook') !== -1 && current.search('merchant') !== -1 ) {
            referrer = $state.params.referrer || $state.current.referrer || 'main';
          }
          else{
            referrer = 'main';
          }
          Auth.logout();
          $state.go(referrer);
        }
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'app/account/signup/signup.html',
        controller: 'SignupController',
        controllerAs: 'vm'
      })
      .state('settings', {
        url: '/settings',
        templateUrl: 'app/account/settings/settings.html',
        controller: 'SettingsController',
        controllerAs: 'vm',
        authenticate: true
      });
  })
  .run(function($rootScope, $stateParams, page, $state) {
    console.log('account referrer',page.getReferrer());
    $rootScope.$on('$stateChangeStart', function(event, next, nextParams, current) {
      if (next.name === 'logout' && current && current.name && !current.authenticate) {
        //next.referrer = current.name;
         if(_.isEmpty($stateParams)){
          next.referrer = current.name;
          }
      }
    });
  });
