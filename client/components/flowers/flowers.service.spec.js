'use strict';

describe('Service: flowers', function () {

  // load the service's module
  beforeEach(module('caribbeanflowershopApp.flowers'));

  // instantiate service
  var flowers;
  beforeEach(inject(function (_flowers_) {
    flowers = _flowers_;
  }));

  it('should do something', function () {
    !!flowers.should.be.true;
  });

});
