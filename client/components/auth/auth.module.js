'use strict';

angular.module('caribbeanflowershopApp.auth', ['caribbeanflowershopApp.constants',
    'caribbeanflowershopApp.util', 'ngCookies', 'ui.router'
  ])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
