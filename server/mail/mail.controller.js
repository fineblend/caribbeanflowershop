'use strict'

var nodemailer = require('nodemailer');
var fs = require('fs');
var hbs= require('nodemailer-express-handlebars');
var rest = require('restler');
var moment = require('moment');
var dollars = require('us-dollars');
import path from 'path';

var amount, country, items, orderId, orderDate, name, delivery, recipient, recipientObject, deliveryObject;

var smtpConfig =   {
  host: 'smtp.mailgun.org',
  port: 465,
  secure: true, // use SSL
  auth: {
    user: 'info@mg.caribbeanflowershop.com',
    pass: 'mailgun'
  }
};

var transporter = nodemailer.createTransport(smtpConfig);

export function deliverMail(req, res, next){
  console.log('order items');
  orderDate = moment(new Date()).format("D MMM YYYY");
  amount = dollars(req.body.amount, 2);
  country = req.body.order.country;
  orderId = req.body.orderId;
  name = req.body.order.name;
  items = req.body.order.items;
  recipientObject = req.body.order.recipient;
  deliveryObject = req.body.order.delivery;
  recipient = formatRecipient(recipientObject);
  delivery = formatDeliveryInfo(deliveryObject);



  transporter.use('compile', hbs({
    viewPath : 'server/views',
    extName: '.html'
  }));

  var mailOptions = {
    from: '"Caribbeanflowershop" <info@caribbeanflowershop.com>', // sender address
    to: 'fineblend@gmail.com', // list of receivers
    subject: 'Caribbeanflowershop test email ✔', // Subject line
    //text: 'Hello world 🐴', // plaintext body
    template: 'email',
    context:{
      orderDate : orderDate,
      name: name,
      items : items,
      total : amount,
      orderId : orderId,
      recipient : recipient,
      delivery: delivery,
      country : country
    }
  };

  transporter.sendMail(mailOptions, function(error, response){
    if(error){
      return console.log(error);
    }else{
      console.log('mail sent', response);
      rest.post('http://www.caribbeanflowershop.com/api/messenger/order', {
        data: {
          items : JSON.stringify(items),
          orderDate : orderDate,
          recipient : recipient,
          delivery : delivery,
          country: country
        }
      });

      res.end('order sent to messenger');
    }
  })
}

export function contact(req, res){
  var name =  req.body.name,
      title = req.body.title,
      email = req.body.email,
      text  = req.body.text;


  var mailOptions = {
    from: name+'"Caribbeanflowershop contact form" <'+email+'>', // sender address
    to: 'fineblend@gmail.com', // list of receivers
    subject: title, // Subject line
    text: text // plaintext body
    };

  transporter.sendMail(mailOptions, function(error, response){
    if(error){
      res.json(error);
    }
    else{
      res.status(200).json(response);
    }
  });

}

function formatRecipient(recipientObject){
  if(!recipientObject){
    return 'John Brown';
  }
    return 'Jack Black';
}

function formatDeliveryInfo(deliveryObject){
  if(!deliveryObject){
    return 'Delivery Date'
  }
    return 'Friday 5th March'
}


