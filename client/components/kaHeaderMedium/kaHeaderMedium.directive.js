'use strict';

angular.module('caribbeanflowershopApp')
  .directive('kaHeaderMedium', function () {
    return {
      templateUrl: 'components/kaHeaderMedium/kaHeaderMedium.html',
      restrict: 'EA',
      scope : {
        heading : '=',
        style : '='
      },
      // link: function (scope, element, attrs) {
      // }
    };
  });
