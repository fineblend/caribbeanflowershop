'use strict';

var app = require('../..');
import request from 'supertest';

var newMessenger;

describe('Messenger API:', function() {
  describe('GET /api/messenger', function() {
    var messengers;

    beforeEach(function(done) {
      request(app)
        .get('/api/messenger')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          messengers = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      messengers.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/messenger', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/messenger')
        .send({
          name: 'New Messenger',
          info: 'This is the brand new messenger!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newMessenger = res.body;
          done();
        });
    });

    it('should respond with the newly created messenger', function() {
      newMessenger.name.should.equal('New Messenger');
      newMessenger.info.should.equal('This is the brand new messenger!!!');
    });
  });

  describe('GET /api/messenger/:id', function() {
    var messenger;

    beforeEach(function(done) {
      request(app)
        .get(`/api/messenger/${newMessenger._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          messenger = res.body;
          done();
        });
    });

    afterEach(function() {
      messenger = {};
    });

    it('should respond with the requested messenger', function() {
      messenger.name.should.equal('New Messenger');
      messenger.info.should.equal('This is the brand new messenger!!!');
    });
  });

  describe('PUT /api/messenger/:id', function() {
    var updatedMessenger;

    beforeEach(function(done) {
      request(app)
        .put(`/api/messenger/${newMessenger._id}`)
        .send({
          name: 'Updated Messenger',
          info: 'This is the updated messenger!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedMessenger = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMessenger = {};
    });

    it('should respond with the original messenger', function() {
      updatedMessenger.name.should.equal('New Messenger');
      updatedMessenger.info.should.equal('This is the brand new messenger!!!');
    });

    it('should respond with the updated messenger on a subsequent GET', function(done) {
      request(app)
        .get(`/api/messenger/${newMessenger._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let messenger = res.body;

          messenger.name.should.equal('Updated Messenger');
          messenger.info.should.equal('This is the updated messenger!!!');

          done();
        });
    });
  });

  describe('PATCH /api/messenger/:id', function() {
    var patchedMessenger;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/messenger/${newMessenger._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Messenger' },
          { op: 'replace', path: '/info', value: 'This is the patched messenger!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedMessenger = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedMessenger = {};
    });

    it('should respond with the patched messenger', function() {
      patchedMessenger.name.should.equal('Patched Messenger');
      patchedMessenger.info.should.equal('This is the patched messenger!!!');
    });
  });

  describe('DELETE /api/messenger/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/messenger/${newMessenger._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when messenger does not exist', function(done) {
      request(app)
        .delete(`/api/messenger/${newMessenger._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
