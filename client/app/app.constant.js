(function(angular, undefined) {
'use strict';

angular.module('caribbeanflowershopApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);