'use strict';

angular.module('caribbeanflowershopApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('merchant', {
        url: '/merchant',
        abstract: true,
        authenticate: 'merchant',
        template: '<merchant></merchant>',
        resolve:{
          'check':function($location, Auth){
            Auth.getCurrentUser().$promise.
            then(response=>{
              var role = response.role;
              if(role !=='merchant'){
                $location.path('/');
              }
            });

          }
        }
      })
      .state('merchant.dashboard',{
        url:'',
        parent: 'merchant',
        templateUrl:'app/account/merchant/merchant_dashboard.html'
      })
      .state('merchant.profile',{
        url:'/merchantprofile',
        parent: 'merchant',
        templateUrl:'app/account/merchant/merchant_profile.html'
      })
      .state('merchant.revenue',{
        url:'/merchantrevenue',
        parent: 'merchant',
        templateUrl:'app/account/merchant/merchant_revenue.html'
      })
      .state('merchant.share',{
        url:'/merchantshare',
        parent: 'merchant',
        templateUrl:'app/account/merchant/merchant_share.html'
      })
      .state('merchant.orders',{
        url:'/merchantorders',
        parent: 'merchant',
        templateUrl:'app/account/merchant/merchant_orders.html'
      })
  });
