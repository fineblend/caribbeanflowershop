'use strict';

describe('Directive: headerMain', function () {

  // load the directive's module and view
  beforeEach(module('caribbeanflowershopApp'));
  beforeEach(module('components/headerMain/headerMain.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<header-main></header-main>');
    element = $compile(element)(scope);
    scope.$apply();
    element.text().should.equal('this is the headerMain directive');
  }));
});
