'use strict';

angular.module('caribbeanflowershopApp')
  .directive('headerSmall', function () {
    return {
      templateUrl: 'components/headerSmall/headerSmall.html',
      restrict: 'EA',
      scope:{
        heading : '=',
        heading2 : '=',
        style : '='
      }
    };
  });
