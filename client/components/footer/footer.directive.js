'use strict';

angular.module('caribbeanflowershopApp')
  .directive('footer', function() {
    return {
      templateUrl: 'components/footer/footer.html',
      restrict: 'E',
      link: function(scope, element, attrs, controller) {
        element.addClass('footer');
      }
    };
  });
