'use strict';

(function() {

  class MainController {

    constructor($http, flowersService, $resource) {
      this.$http = $http;
      this.$resource = $resource;
      this.flowersService = flowersService;
      this.heading = 'Choose Flowers';
      this.heading2 = 'We deliver in several Caribbean countries using local florists. With our service you can send with the confidence that your flowers will arrive fresh and on time.';
      this.flowers = [];
      this.blog = {};
      this.totalItems = 100;
      this.currentPage = 1;
      this.itemsPerPage = 3;
      this.maxSize = 5;
      this.numPages = this.totalItems /  this.itemsPerPage;
      // slider
      this.active = 0;
      this.interval = 10000;
      this.noWrapSlides = false;

    }

    setPage(pageNo){
      this.currentPage = pageNo;
    }

    scroll(){
      var scrollTop = 2000 // For example
      $('html, body').animate({scrollTop: $('#section2').offset().top}, 'slow');
    }


    $onInit() {
      this.flowersService.getFlowerResource().query().$promise
        .then(response => {
          this.flowers = response;
          this.flowers.forEach(function(element){
            element.price = Number(element.price);
            element.shareASaleLink = element.shareASaleLink.replace(/YOURUSERID/i, '166025');
          });
          this.totalItems = response.length;
        });

      this.$http({
        method: 'GET',
        url: 'https://blog.caribbeanflowershop.com/api/post',
        isArray:true
      })
        .success(data=>{
          this.blog = data;
          var lengthMinus2 = this.blog.length - 2;
          this.blog.splice(0, lengthMinus2);
          this.blog.forEach(function(element){
            element._id = Number(element._id);
            element.body = element.body.slice(0,200) +'.......';
          });
        })
        .error(error=>{
          console.log('error', error);
        });

    }



  }

  angular.module('caribbeanflowershopApp')
    .component('main', {
      templateUrl: 'app/main/main.html',
      controller: MainController,
      controllerAs: 'main'
    });
})();
