'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var messengerCtrlStub = {
  index: 'messengerCtrl.index',
  show: 'messengerCtrl.show',
  create: 'messengerCtrl.create',
  upsert: 'messengerCtrl.upsert',
  patch: 'messengerCtrl.patch',
  destroy: 'messengerCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var messengerIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './messenger.controller': messengerCtrlStub
});

describe('Messenger API Router:', function() {
  it('should return an express router instance', function() {
    messengerIndex.should.equal(routerStub);
  });

  describe('GET /api/messenger', function() {
    it('should route to messenger.controller.index', function() {
      routerStub.get
        .withArgs('/', 'messengerCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe.skip('GET /api/messenger/:id', function() {
    it('should route to messenger.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'messengerCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe.skip('POST /api/messenger', function() {
    it('should route to messenger.controller.create', function() {
      routerStub.post
        .withArgs('/', 'messengerCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/messenger/:id', function() {
    it('should route to messenger.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'messengerCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/messenger/:id', function() {
    it('should route to messenger.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'messengerCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/messenger/:id', function() {
    it('should route to messenger.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'messengerCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
