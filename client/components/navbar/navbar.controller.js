'use strict';

class NavbarController {
  //end-non-standard

  //start-non-standard
  constructor(Auth, $rootScope, $state, cartService) {
    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
    this.$state = $state;
    this.$rootScope = $rootScope;
    this.cartService = cartService;
    this.totalItems;
    this.menu = [{
      'title': 'Home',
      'state': 'main',
    },
      {
        'title': 'About',
        'state': 'about'
      }
      ];
  }


   goRole() {
     if(this.isLoggedIn()){
       var role = this.getCurrentUser().role;
       if(role ==='user'){
          this.$state.go('settings');
       }else{
         this.$state.go(role);
         this.role = role;
       }
     }

   }

   watchTotalItems(){
     this.$rootScope.$on('cartChange', ()=>{
       if(this.cartService.getTotalItems() > 0)
       this.totalItems=this.cartService.getTotalItems();
     })

   }

  $onInit(){
    this.goRole();
    if(this.cartService.getTotalItems() > 0){
      this.totalItems = this.cartService.getTotalItems();
    }
    this.watchTotalItems();
  }



}

angular.module('caribbeanflowershopApp')
  .controller('NavbarController', NavbarController);
