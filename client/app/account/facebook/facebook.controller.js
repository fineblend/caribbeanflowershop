'use strict';

(function(){

class FacebookComponent {
  constructor(Auth, $http, $location, $state, page) {
    this.$http = $http;
    this.$location = $location;
    this.$state = $state;
    this.Auth = Auth;
    this.page = page;
    this.user = {};
    this.role = '';
    this.name = '';
    this.facebook = {};
    this.picture = '';
    this.heading = 'Dashboard';
    this.style = '{\'background-image\': \'url(assets/images/trinidad.jpg)\',      \'background-repeat\':\'no-repeat\',\'background-size\':\'cover\'}';
  }


  setProfile(){
    this.Auth.createUser(this.user, (error, response)=>{
        if(error){
          return console.log(error)
        }else{
          console.log(response);
          this.Auth.getCurrentUser().$promise
            .then(data=> {
              return this.user = data;
            })
        }
    });
  }

  setRole(role){
    this.user.role = role;
    if(!this.user.arrangements){
      this.user.arrangements = [];
    }
    var id = this.user._id;
    this.$http({
      method: 'PUT',
      url: '/api/users/' + id,
      data: this.user
    })
      .then(response =>{
          console.log(response);
          this.$location.path('/merchant');
        },
        error=> console.log(error));


    // this.Auth.createUser(this.user, (error, response)=>{
    //   if(error){
    //     return console.log(error);
    //   }
    //   else{
    //     this.Auth.getCurrentUser().$promise.
    //       then(this.$location.path('/merchant'));
    //   }
    // });
  }

  showRole(role){
    this.role = role;
  }

  $onInit(){
    var referrer = this.page.getReferrer();
    if(referrer === 'cart'){
      this.page.setReferrer(null);
      console.log('page referrer' , this.page.getReferrer());
      this.$state.go('cart.checkout');
    }

    this.Auth.getCurrentUser().$promise
      .then(response=>{
        this.user = response;
        this.role = response.role;
        this.picture = response.facebook.picture.data.url;
        this.facebook = response.facebook;
        this.name = response.name.split(' ')[0];
        if(response.facebook.cover){
          var coverPhoto = response.facebook.cover.source;
          this.style =  '{\'background-image\': \'url('+coverPhoto+')\',      \'background-repeat\':\'no-repeat\',\'background-size\':\'cover\'}';
        }
    });
  }

}

angular.module('caribbeanflowershopApp')
  .component('facebook', {
    templateUrl: 'app/account/facebook/facebook.html',
    controller: FacebookComponent,
    controllerAs: 'fb'
  });

})();

