'use strict';

(function(){

class CountryComponent {
  constructor($resource, $rootScope, $stateParams, toastr, cartService) {
    this.$resource = $resource;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
    this.toastr = toastr;
    this.cartService = cartService;
    this.heading = 'Choose Flowers';
    this.heading2 = 'A beautiful option awaits';
    this.flowers = [];
    this.totalItems = 0;
    this.currentPage = 1;
    this.itemsPerPage = 9;
    this.maxSize = 5;
    this.numPages = this.totalItems /  this.itemsPerPage;
    this.chosenCountry = '';
    this.totalCartItems = '0';

  }

  //either send to main or offsite list
  addItemToCart(item){

      var id = item._id;
      var price = item.price;
      var qty = 1;
      var merchant;
      console.log(item);
      if((item.merchants) && (item.merchants.length>0)){
        console.log('merchant', item.merchants);
        merchant = 'merchant';
      }else{
        console.log('merchant test failed', item);
        merchant = 'other';
      }
      var cartItem = {id:id, price:price, qty: qty, merchant: merchant, item:item};
      if(cartItem.merchant === 'merchant'){
        this.cartService.addItemToCart(cartItem);
      }

      if(cartItem.merchant === 'other'){
        var offSiteSuppliers;
        if(localStorage.getItem('offSiteSuppliers')){
          offSiteSuppliers = JSON.parse(localStorage.getItem('offSiteSuppliers'));
        }else{
          offSiteSuppliers =[];
        }
        offSiteSuppliers.push(cartItem);
        this.offSiteItems = offSiteSuppliers;
        localStorage.setItem('offSiteSuppliers', JSON.stringify(offSiteSuppliers));
      }

      this.toastr.info(item.name+' added to cart');
  }

  getCountryInfo(){
    var chosenCountry = this.$stateParams.country;
    this.chosenCountry = chosenCountry;
    var flowers = this.$resource('/api/flowers/country/:country',{country:'@country'});
    flowers.query({country: chosenCountry})
      .$promise
      .then(response => {
            this.flowers = response;
            this.flowers.forEach(function(element){
              element.price = Number(element.price);
              element.shareASaleLink = element.shareASaleLink.replace(/YOURUSERID/i, '166025');
            });
            this.totalItems = response.length;
          });
  }

  $onInit() {
    this.getCountryInfo();
  }


}

angular.module('caribbeanflowershopApp')
  .component('country', {
    templateUrl: 'app/country/country.html',
    controller: CountryComponent,
    controllerAs: 'country'
  });

})();
