'use strict';

function paginateFilter() {
  return function (flowers, currentPage, itemsPerPage) {
    return flowers.slice(((currentPage-1)*itemsPerPage), ((currentPage)*itemsPerPage));

  };
}


angular.module('caribbeanflowershopApp')
  .filter('paginate', paginateFilter);
