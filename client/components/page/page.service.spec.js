'use strict';

describe('Service: page', function () {

  // load the service's module
  beforeEach(module('caribbeanflowershopApp'));

  // instantiate service
  var page;
  var $rootScope;
  beforeEach(inject(function (_page_, _$rootScope_) {
    page = _page_;
    $rootScope = _$rootScope_;
  }));

  it('check service instantiates', function () {
    page.should.exists;
  });
  it('getCountryArray returns an array with Aruba', function(){
    page.getCountryArray().then(response=>{
      response.should.contain('Aruba');
    });
    $rootScope.$apply();
  });

});
